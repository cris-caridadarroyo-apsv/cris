package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher r = (Researcher) request.getSession().getAttribute("user");
			String id = request.getParameter("id");
			String publicationName = request.getParameter("publicationName");
			String title = request.getParameter("title");
			String publicationDate = request.getParameter("publicationDate");
			String authors = request.getParameter("authors");
			Publication p1 = new Publication();
			p1.setId(id);
			p1.setPublicationName(publicationName);
			p1.setTitle(title);
			p1.setPublicationDate(publicationDate);
			p1.setAuthors(authors);
	        Client client = ClientBuilder.newClient(new ClientConfig());
			Response resp = client.register(JsonProcessingFeature.class)
					.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/").request()
					                .post(Entity.entity(p1, MediaType.APPLICATION_JSON), Response.class);
			response.sendRedirect(request.getContextPath() + "/ResearcherServlet");
		}
	}

